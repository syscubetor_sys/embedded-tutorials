/*
 * commI2c.h
 *
 *  Created on: 04-Sep-2020
 *      Author: syscubetor
 */

#ifndef INC_COMMI2C_H_
#define INC_COMMI2C_H_

#include "stm32f0xx_hal.h"
#ifdef __cplusplus
extern "C" {
#endif

#define HAL_I2C_MAX_TIMEOUT 0xFFFF
#define HAL_I2C_READ_TIMEOUT 0xFF
#define HAL_I2C_WRITE_TIMEOUT 0xFF
#define MAX_I2C_READ_BUFFER_LEN 0xFF
#define MAX_I2C_WRITE_BUFFER_LEN 0xFF

void commI2c_Init(void);
HAL_StatusTypeDef commI2c_Write(uint16_t addr, const uint8_t *txBuff, uint32_t txDataLen);
HAL_StatusTypeDef commI2c_Read(uint16_t addr, uint8_t *rxBuff, uint32_t rxDataLen);

#ifdef __cplusplus
}
#endif


#endif /* INC_COMMI2C_H_ */
