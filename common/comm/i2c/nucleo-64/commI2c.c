/*syscubetor
 * commI2c.c
 *
 *  Created on: 04-Sep-2020
 *      Author: syscubetor
 */

#include <stdio.h>
#include <console.h>
#include <string.h>
#include <commI2c.h>
#include "stm32f0xx_hal.h"

I2C_HandleTypeDef hi2c1;

void commI2c_Init(void)
{

	hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  
  HAL_I2C_Init(&hi2c1);

  /** Configure Analogue filter
  */
  HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE);
  
  /** Configure Digital filter
  */
  HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0);

}

HAL_StatusTypeDef commI2c_Write(uint16_t addr, const uint8_t *txBuff, uint32_t txDataLen)
{
	if(txBuff == NULL )
  {
    printf("invalid write buffer");
		return HAL_ERROR;
  }
	
  return  HAL_I2C_Master_Transmit(&hi2c1,addr,txBuff,txDataLen,HAL_I2C_WRITE_TIMEOUT);

}

HAL_StatusTypeDef commI2c_Read(uint16_t addr, uint8_t *rxBuff, uint32_t rxDataLen)
{
	if(rxBuff == NULL || rxDataLen == 0)
  {
    printf("invalid read buffer or buffer size");
    return HAL_ERROR;
  }
  return HAL_I2C_Master_Receive(&hi2c1,addr,rxBuff,rxDataLen,HAL_I2C_READ_TIMEOUT);

}


