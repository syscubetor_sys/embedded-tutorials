/*
 * commSpi.h
 *
 *  Created on: 04-Sep-2020
 *      Author: syscubetor
 */

#ifndef INC_COMMSPI_H_
#define INC_COMMSPI_H_

#include "stm32f0xx_hal.h"
#ifdef __cplusplus
extern "C" {
#endif

#define HAL_SPI_MAX_TIMEOUT 0xFFFF
#define HAL_SPI_READ_TIMEOUT 0xFF
#define HAL_SPI_WRITE_TIMEOUT 0xFF
#define MAX_SPI_READ_BUFFER_LEN 0xFF
#define MAX_SPI_WRITE_BUFFER_LEN 0xFF

void commSpi_Init(void);
HAL_StatusTypeDef commSpi_Write(uint8_t *txBuff, uint32_t txDataLen);
HAL_StatusTypeDef commSpi_Read(uint8_t *rxBuff, uint32_t rxDataLen);
HAL_StatusTypeDef commSpi_WriteRead(uint8_t *txBuff, uint32_t txDataLen, uint8_t *rxBuff, uint32_t rxDataLen);

#ifdef __cplusplus
}
#endif


#endif /* INC_COMMSPI_H_ */
