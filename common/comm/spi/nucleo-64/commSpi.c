/*
 * commSpi.c
 *
 *  Created on: 04-Sep-2020
 *      Author: syscubetor
 */

#include <stdio.h>
#include <console.h>
#include <string.h>
#include "commSpi.h"
#include "stm32f0xx_hal.h"

#define ADXL345_CS_ASSERT() 	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET)
#define ADXL345_CS_DEASSERT()   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET)

SPI_HandleTypeDef hspi2;

void commSpi_Init(void)
{

  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi2.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 7;
  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi2.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  
  HAL_SPI_Init(&hspi2);


}

HAL_StatusTypeDef commSpi_Write(uint8_t *txBuff, uint32_t txDataLen)
{
	HAL_StatusTypeDef ret = HAL_ERROR;
	if(txBuff == NULL )
  {
    printf("invalid write buffer");
		return HAL_ERROR;
  }
	// Setting R/W = 0, i.e.: Write Mode
	txBuff[0] &= ~(0x80);
	ADXL345_CS_ASSERT();
	ret = HAL_SPI_Transmit(&hspi2, txBuff, txDataLen, HAL_SPI_WRITE_TIMEOUT);
	ADXL345_CS_DEASSERT();
	return ret;
  //return  HAL_SPI_Transmit(&hspi2, txBuff, txDataLen, HAL_SPI_WRITE_TIMEOUT);

}

HAL_StatusTypeDef commSpi_Read(uint8_t *rxBuff, uint32_t rxDataLen)
{
	HAL_StatusTypeDef ret =HAL_ERROR;
	if(rxBuff == NULL || rxDataLen == 0)
  {
    printf("invalid read buffer or buffer size");
    return HAL_ERROR;
  }
	ADXL345_CS_ASSERT();
	ret = HAL_SPI_Receive(&hspi2, rxBuff, rxDataLen, HAL_SPI_READ_TIMEOUT);

	ADXL345_CS_DEASSERT();
  return ret;
  //return HAL_SPI_Receive(&hspi2, rxBuff, rxDataLen, HAL_SPI_READ_TIMEOUT);

}

HAL_StatusTypeDef commSpi_WriteRead(uint8_t *txBuff, uint32_t txDataLen , uint8_t *rxBuff, uint32_t rxDataLen)
{
  HAL_StatusTypeDef ret =HAL_ERROR;
  if(rxBuff == NULL || rxDataLen == 0)
  {
    printf("invalid read buffer or buffer size");
    return HAL_ERROR;
  }
  // Multiple Byte Read Settings
   if (rxDataLen > 1)
     txBuff[0] |= 0x40;
   else
     txBuff[0] &= ~(0x40);
   // Setting R/W = 1, i.e.: Read Mode
   txBuff[0] |= (0x80);
  ADXL345_CS_ASSERT();
  ret = HAL_SPI_Transmit(&hspi2, txBuff, txDataLen, HAL_SPI_WRITE_TIMEOUT);
  if(ret != HAL_OK){
	  printf("transmit failed %s", __FUNCTION__);
	  ADXL345_CS_DEASSERT();
	  return ret ;
  }

  ret = HAL_SPI_Receive(&hspi2, rxBuff, rxDataLen, HAL_SPI_READ_TIMEOUT);
  ADXL345_CS_DEASSERT();
  return ret;
  //return HAL_SPI_Receive(&hspi2, rxBuff, rxDataLen, HAL_SPI_READ_TIMEOUT);

}




