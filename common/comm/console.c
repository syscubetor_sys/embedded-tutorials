#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "stm32f0xx_hal.h"
#include "console.h"

UART_HandleTypeDef huart2;
static uint8_t buffer[MAX_READ_BUFFER_LEN];

void Dbg_Init(void)
{

	 huart2.Instance = USART2;
	  huart2.Init.BaudRate = 115200;
	  huart2.Init.WordLength = UART_WORDLENGTH_8B;
	  huart2.Init.StopBits = UART_STOPBITS_1;
	  huart2.Init.Parity = UART_PARITY_NONE;
	  huart2.Init.Mode = UART_MODE_TX_RX;
	  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	  HAL_UART_Init(&huart2) ;

}

void Dbg_Printf(const char * format, ...)
{
	char printBuffer[500] = {0};
	uint32_t buffSize = 0 ;
	uint32_t printSize = 0 ;
	va_list vArgs;
	va_start(vArgs, format);
	buffSize = sizeof(printBuffer) / sizeof(printBuffer[0]) -1 ;
	printSize = vsnprintf(printBuffer, buffSize, format, vArgs);
	va_end(vArgs);

	HAL_UART_Transmit(&huart2, (uint8_t *)printBuffer, printSize, HAL_MAX_TIMEOUT);
	HAL_UART_Transmit(&huart2, (uint8_t *)"\r\n", 2, HAL_MAX_TIMEOUT);

}
void Dbg_Scanf(const char * formatString, ...)
{
	va_list vArgs;
	char scanBuffer[MAX_READ_BUFFER_LEN] = {0};
	uint8_t buffIndx = 0;
	memset(buffer, 0, sizeof(buffer));

start:
	HAL_UART_Receive(&huart2, (uint8_t *)&buffer[buffIndx], 1, HAL_READ_TIMEOUT);
	if(buffer[buffIndx] != '\r' && buffer[buffIndx] != '\n')
	{
		buffIndx++;
		goto start;
	}
	memcpy(scanBuffer,buffer,buffIndx);
	va_start(vArgs , formatString );
	vsscanf(scanBuffer, formatString, vArgs );
	va_end(vArgs);
}

