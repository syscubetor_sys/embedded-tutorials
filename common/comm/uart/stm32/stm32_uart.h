#ifndef __STM32_UART_H__
#define __STM32_UART_H__

#include "stm32f407xx.h"
#include "stm32f4xx_hal_def.h"
#include "stm32f4xx_hal_uart.h"
#include <stdint.h>

HAL_StatusTypeDef stm32_uartInit(UART_HandleTypeDef *uart,
                                 USART_TypeDef *baseaddr);
HAL_StatusTypeDef stm32_uartSend(UART_HandleTypeDef *pUart, uint8_t *ptxData,
                                 uint16_t txDataLen);
HAL_StatusTypeDef stm32_uartReceive(UART_HandleTypeDef *pUart, uint8_t *prxData,
                                    uint16_t *prxDataLen);

#endif /* __STM32_UART_H__ */
