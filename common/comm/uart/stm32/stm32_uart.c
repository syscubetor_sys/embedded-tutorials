#include <stdio.h>

#include "stm32_uart.h"

#define HAL_MAX_TIMEOUT 0xFFu

HAL_StatusTypeDef stm32_uartInit(UART_HandleTypeDef *pUart,
                                 USART_TypeDef *baseaddr)
{
    /**USART2 GPIO Configuration
      PA2     ------> USART2_TX
      PA3     ------> USART2_RX
      */
    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_InitStructure.Pin       = GPIO_PIN_2;
    GPIO_InitStructure.Mode      = GPIO_MODE_AF_PP;
    GPIO_InitStructure.Alternate = GPIO_AF7_USART2;
    GPIO_InitStructure.Speed     = GPIO_SPEED_HIGH;
    GPIO_InitStructure.Pull  = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
        
    GPIO_InitStructure.Pin     = GPIO_PIN_3;
    GPIO_InitStructure.Mode = GPIO_MODE_AF_OD;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);

    __USART2_CLK_ENABLE();

    pUart->Instance        = baseaddr;
    pUart->Init.BaudRate   = 115200;
    pUart->Init.WordLength = UART_WORDLENGTH_8B;
    pUart->Init.StopBits   = UART_STOPBITS_1;
    pUart->Init.Parity     = UART_PARITY_NONE;
    pUart->Init.Mode       = UART_MODE_TX_RX;
    pUart->Init.HwFlowCtl  = UART_HWCONTROL_NONE;
    // pUart->Init.OverSampling = UART_OVERSAMPLING_16;

    return HAL_UART_Init(pUart);
}

HAL_StatusTypeDef stm32_uartSend(UART_HandleTypeDef *pUart, uint8_t *ptxData,
                                 uint16_t txDataLen)
{
    HAL_StatusTypeDef ret = HAL_ERROR;
    if (ptxData == NULL)
    {
        return HAL_ERROR;
    }
    if (txDataLen < 1)
    {
        return HAL_ERROR;
    }
    ret = HAL_UART_Transmit(pUart, ptxData, txDataLen, HAL_MAX_TIMEOUT);

    return ret;
}

HAL_StatusTypeDef stm32_uartReceive(UART_HandleTypeDef *pUart, uint8_t *prxData,
                                    uint16_t *prxDataLen)
{
    HAL_StatusTypeDef ret = HAL_ERROR;
    ret = HAL_UART_Receive(pUart, prxData, *prxDataLen, HAL_MAX_TIMEOUT);
    return ret;
}
