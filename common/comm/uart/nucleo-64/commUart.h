/*
 * commUart.h
 *
 *  Created on: 04-Sep-2020
 *      Author: syscubetor
 */

#ifndef INC_COMMUART_H_
#define INC_COMMUART_H_

#include "stm32f0xx_hal.h"
#ifdef __cplusplus
extern "C" {
#endif

#define HAL_MAX_TIMEOUT 0xFF
#define HAL_READ_TIMEOUT 0xFFFF
#define MAX_READ_BUFFER_LEN 0xFF
#define MAX_WRITE_BUFFER_LEN 0xFF

void commUart_Init(void);
HAL_StatusTypeDef commUart_Write(const uint8_t *txBuff, uint32_t txDataLen);
HAL_StatusTypeDef commUart_Read(uint8_t *rxBuff, uint32_t *rxDataLen);

#ifdef __cplusplus
}
#endif


#endif /* INC_COMMUART_H_ */
