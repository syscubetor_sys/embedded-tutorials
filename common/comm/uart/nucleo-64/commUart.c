/*syscubetor
 * commUart.c
 *
 *  Created on: 04-Sep-2020
 *      Author: syscubetor
 */

#include <stdio.h>
#include <commUart.h>
#include <console.h>
#include <string.h>
#include "stm32f0xx_hal.h"

UART_HandleTypeDef huart1;

void commUart_Init(void)
{

	huart1.Instance = USART1;
	  huart1.Init.BaudRate = 9600;
	  huart1.Init.WordLength = UART_WORDLENGTH_8B;
	  huart1.Init.StopBits = UART_STOPBITS_1;
	  huart1.Init.Parity = UART_PARITY_NONE;
	  huart1.Init.Mode = UART_MODE_TX_RX;
	  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
	  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	  HAL_UART_Init(&huart1);

}

HAL_StatusTypeDef commUart_Write(const uint8_t *txBuff, uint32_t txDataLen)
{
	HAL_StatusTypeDef ret = HAL_ERROR;
	if(txBuff == NULL )
	{
		printf("invalide write buffer");
		return ret;
	}
	ret = HAL_UART_Transmit(&huart1, (uint8_t *)txBuff, txDataLen, HAL_MAX_TIMEOUT);
	ret = HAL_UART_Transmit(&huart1, (uint8_t *)"\r\n", 2, HAL_MAX_TIMEOUT);
	return ret;

}
HAL_StatusTypeDef commUart_Read(uint8_t *rxBuff, uint32_t *rxDataLen)
{
	if(rxBuff == NULL)
	{
		printf("invalide read buffer");
		return HAL_ERROR;
	}

	return (HAL_UART_Receive_IT(&huart1, rxBuff, 4));

}


