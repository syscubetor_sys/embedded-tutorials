

#ifndef INC_CONSOLE_H_
#define INC_CONSOLE_H_

#ifdef __cplusplus
extern "C" {
#endif

#define HAL_MAX_TIMEOUT 0xFF
#define HAL_READ_TIMEOUT 0xFFFF
#define MAX_READ_BUFFER_LEN 0xFF
#define MAX_WRITE_BUFFER_LEN 0xFF

#define printf Dbg_Printf
#define scanf Dbg_Scanf

void Dbg_Init(void);
void Dbg_Printf(const char * format, ...);
void Dbg_Scanf(const char * formatString, ...);

#ifdef __cplusplus
}
#endif

#endif /* INC_CONSOLE_H_ */
