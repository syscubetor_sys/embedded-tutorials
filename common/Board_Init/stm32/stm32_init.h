#ifndef __STM32_INIT_H__
#define __STM32_INIT_H__

void stm32_clockInit(void);
void stm32_gpioClkInit(void);
void stm32_bootInit(void);
void SysTick_Handler(void);

#endif /*__STM32_INIT_H__*/
