.. Embedded Systems documentation master file, created by
   sphinx-quickstart on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Demo Application of I2C
===================================================

Below example showcase actual working of I2C protocol. This application is based on Nucleo-64 

Prerequisites
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	1. STM32Cube IDE installed
	2. ADXL345 module

Code gothrough
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. literalinclude:: ../../../projects/nucleo-64/baremetal/Core/app-i2c/app-i2c.c
   :language: c
   :start-after: /*doc:start*/
   :end-before: /*doc:end*/

Output
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image :: ../../images/i2c/i2c_demo.jpg

Realtime Traces
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
I2C write operation Trace:

.. image :: ../../images/i2c/i2c_demo_trace_write.jpg

I2C read operation Trace

.. image :: ../../images/i2c/i2c_demo_trace_read.jpg