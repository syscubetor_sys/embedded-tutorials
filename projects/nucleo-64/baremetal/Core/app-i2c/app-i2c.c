/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
/*doc:start*/
#include "main.h"
#include "console.h"
#include <math.h>
#include "commI2c.h"

#define ADXL345_Address 0x53<<1

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void ADXL345_Init(void);
/* Private user code ---------------------------------------------------------*/

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
	int16_t ax,ay,az;
	float Xaccel=0.0, Yaccel=0.0, Zaccel=0.0;
	uint8_t i2c_buf[10] = {0};


  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  commI2c_Init();
  Dbg_Init();
  ADXL345_Init();

  printf("***************************************");
  printf("Demo Example of ADXL345 I2C");
  printf("***************************************");
  HAL_Delay(100);


  while (1)
  {

    //To Read Accelerometer Data
    i2c_buf[0] = 0x32; //Accelerometer data from 0x32
    commI2c_Write(ADXL345_Address,&i2c_buf[0],1);

    commI2c_Read(ADXL345_Address,&i2c_buf[0],6);
    ax = ((i2c_buf[1]<<8)|i2c_buf[0]);
    ay = ((i2c_buf[3]<<8)|i2c_buf[2]);
    az = ((i2c_buf[5]<<8)|i2c_buf[4]);

    Xaccel = ax * .0078;
    Yaccel = ay * .0078;
    Zaccel = az * .0078;

    printf("Xaccel :%f   Yaccel:%f   Zaccel:%f\n", Xaccel,Yaccel,Zaccel);

	  HAL_Delay(100);
  }
  /* USER CODE END 3 */
}
/*doc:end*/

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}


/* USER CODE BEGIN 4 */
static void ADXL345_Init(void)
{
	uint8_t i2c_txBuff[2] = {0};
  /*set data_format range= +- 4g */
	i2c_txBuff[0] = 0x31 ;
	i2c_txBuff[1] = 0x01;
  commI2c_Write(ADXL345_Address,i2c_txBuff,2);
  /*reset all bits*/
  i2c_txBuff[0] = 0x2d;
  i2c_txBuff[1] = 0x00;
  commI2c_Write(ADXL345_Address,i2c_txBuff,2);
  /*set power_cntl measure and wake up 8hz*/
  i2c_txBuff[0] = 0x2d;
  i2c_txBuff[1] = 0x08;
  commI2c_Write(ADXL345_Address,i2c_txBuff,2);

}
/* USER CODE END 4 */
/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
