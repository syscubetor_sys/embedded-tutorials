.. Embedded Systems documentation master file, created by
   sphinx-quickstart on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Demo Application of UART
===================================================

Below example showcase actual working of UART protocol. This application is based on Nucleo-64 
board which uses debug console as COM port.

Prerequisites
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	1. STM32Cube IDE installed
	#. On tera Term or any com emulator software set baud rated to 115200.

Code gothrough
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. literalinclude:: ../../../projects/nucleo-64/baremetal/Core/app-uart/app-uart.c
   :language: c
   :start-after: /*doc:start*/
   :end-before: /*doc:end*/

Output
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image :: ../../images/uart/uart_demo.jpg

