.. Embedded Systems documentation master file, created by
   sphinx-quickstart on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Demo Application of SPI
===================================================

Below example showcase actual working of SPI protocol. This application is based on Nucleo-64 

Prerequisites
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	1. STM32Cube IDE installed
	2. ADXL345 module

Code gothrough
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. literalinclude:: ../../../projects/nucleo-64/baremetal/Core/app-spi/app-spi.c
   :language: c
   :start-after: /*doc:start*/
   :end-before: /*doc:end*/

Output
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image :: ../../images/spi/spi_demo.jpg


Realtime Traces
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image :: ../../images/spi/spi_demo_readwrite.jpg