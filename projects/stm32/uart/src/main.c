#include <stdio.h>

#include "main.h"
#include "stm32_init.h"
#include "stm32_uart.h"
#include "stm32f4xx.h" // Device header
#include "stm32f4xx_hal.h"

#define LED_GREEN GPIO_PIN_12
#define LED_RED GPIO_PIN_14
#define LED_BLUE GPIO_PIN_15

static uint8_t AT[] = "AT";

int main()
{
    UART_HandleTypeDef huart2;
    GPIO_InitTypeDef GPIO_InitStruct;
    HAL_StatusTypeDef ret;
    uint8_t count = 0;
    stm32_bootInit();
    stm32_clockInit();
    stm32_gpioClkInit();
    stm32_ledInit(&GPIO_InitStruct);
    ret = stm32_uartInit(&huart2, USART2);
    if (ret != HAL_OK)
    {
        stm32_app_fail();
        return -1;
    }
    do
    {
        ret = stm32_uartSend(&huart2, AT, sizeof(AT));
        if (ret != HAL_OK)
        {
            stm32_app_fail();
            return -1;
        }
        else
        {
            stm32_app_success();
        }
        HAL_Delay(500);
        count++;
    } while (count == 10);

    return 0;
}

void stm32_ledInit(GPIO_InitTypeDef *GPIO_InitStruct)
{
    GPIO_InitStruct->Pin   = LED_GREEN | LED_RED | LED_BLUE;
    GPIO_InitStruct->Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct->Pull  = GPIO_NOPULL;
    GPIO_InitStruct->Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOD, GPIO_InitStruct);
    HAL_GPIO_WritePin(GPIOD, LED_BLUE, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOD, LED_GREEN | LED_RED, GPIO_PIN_RESET);
}

void stm32_app_fail(void)
{
    HAL_GPIO_WritePin(GPIOD, LED_BLUE | LED_RED | LED_GREEN, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOD, LED_RED, GPIO_PIN_SET);
}

void stm32_app_success(void)
{
    HAL_GPIO_WritePin(GPIOD, LED_BLUE | LED_RED | LED_GREEN, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOD, LED_GREEN, GPIO_PIN_SET);
}
