#ifndef __STM32_LED_INIT_H__
#define __STM32_LED_INIT_H__

#include "stm32f4xx_hal_gpio.h"

void stm32_app_fail(void);
void stm32_app_success(void);
void stm32_ledInit(GPIO_InitTypeDef *GPIO_InitStruct);

#endif /* __STM32_LED_INIT_H__ */