#include <stdio.h>

#include "stm32_init.h"
#include "stm32f4xx.h" // Device header
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"

#define LED_GREEN       GPIO_PIN_12
#define LED_ORANGE      GPIO_PIN_13
#define LED_RED         GPIO_PIN_14
#define LED_BLUE        GPIO_PIN_15

int main()
{
    GPIO_InitTypeDef GPIO_InitStruct;
    stm32_bootInit();
    stm32_clockInit();
    stm32_gpioClkInit();
    GPIO_InitStruct.Pin   = LED_GREEN | LED_ORANGE | LED_RED | LED_BLUE;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
    HAL_GPIO_WritePin(GPIOD, LED_GREEN | LED_ORANGE | LED_RED | LED_BLUE,
                      GPIO_PIN_RESET);
    while (1)
    {
        HAL_GPIO_TogglePin(GPIOD, LED_GREEN | LED_ORANGE | LED_RED | LED_BLUE);
        HAL_Delay(100);
    }

    return 0;
}
