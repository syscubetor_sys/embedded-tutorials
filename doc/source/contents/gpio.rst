.. Embedded Systems documentation master file, created by
   sphinx-quickstart on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



General Purpose Input Output (GPIO)
===================================================

All computers understand digital logic. This means that computers only understand whether a voltage is present or not. 

Consider a microcontroller thatb works on 5V. Which means, when 5V is given to the power pin, the microcontroller will be turned on. This also means that when there is a on a pin, the voltage is at 5V and when there is no signal on the pin, the voltage is at 0V.

Microcontrollers have the ability to set the voltage on any of the pins to either 5V or 0V. Also if an external source is setting a voltage, microcontrollers have the ability to sense whether there is 5V on the pin corresponsinf to a HIGH signal, or a 0V correspomnding to a LOW signal. 

A lot of new microcontrollers also come with 3.3V power. Conceptually they remain the same except that the HIGH voltage is lower.

A lot of the pins on the microcontroller, have a dedicated purpose, for example the power pins are only used to power the circuits. However, GPIO pins are customizable and can be controlled by the software. Since all the signals are HIGH or LOW signals at their core, once you can control the GPIO, you can control anything.

The image shows the GPIO pins on the very popular Arduino Controller.

.. image:: ../../images/embedded/arduino-gpio.jpg
	:align: center
	:width: 400px

Demo Application Using STM32 on Nucleo-64 Development Board
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To try this example, we will need an Nucleo-64 development board, a switch, an LED and 2 resistors. You can select any pins you wish. 

Connect the switch, LED and the resistors to the microcontoller GPIO pins as shown.

IMAGE

If you do not have an LED or a resistor, the Nucleo board has an onboard LED for testing. Should look similar to this. Here you will need to identify the pin where the LED is connected. In the image, the LED is connected to pin D13.

IMAGE

1) **GPIO Registers**

All the pins in the microcontroller are divided into ports. For example, an STM32F401VE  has five ports namely PORTA, PORTB, PORTC, PORTD, PORTE and each port has 16 pins. They are also represented as GPIOA, GPIOB, GPIOC, GPIOD and GPIOE.

We can control the voltages on these GPIO pins by controlling the values inside certain special registers. This register is called the output data register (GPIOx_ODR).

If you need to read the value at any pin, we have to use the input data register (GPIOx_IDR).

A GPIO mode register (GPIOx_MODER) is used to set the pin in either input or output mode.

GPIO is not the only function of the pins. Certain pins can be used for oter functionality. This is known as alternate functionality of the pins. There are 2 registers to determine this namely the Alternate Function Low register (GPIOx_AFRL) and Alternate Function High register (GPIOx_AFRH).

Additionally, there is a clock register (RCC_AHB1ENR) which will enable the AHB clock to the GPIO ports, Output speed register (GPIOx_OSPEEDR) is used to set the speed of the GPIO pin.

These registers are specific to STM32, the general approach remains the same for any microcontroller. Certain registers help control the behavior of the pins. 


2) **Controlling GPIO**
	
First step involves enabling the clock for the microcontroller. In STM32, clock needs to be enabled for the GPIO ports too.

Next we need to set the functionlity of the pins to GPIO and the mode to either input or output. 

After this is done, we can read or write values to the GPIO pins.

3) **LED Control Example**

4) **Switch Read Example**


Program
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Code ::

	/* Includes ------------------------------------------------------------------*/
	#include "main.h"


	/* Private function prototypes -----------------------------------------------*/
	void SystemClock_Config(void);
	static void MX_GPIO_Init(void);


	/* Main function. Execution starts here --------------------------------------*/
	int main(void)
	{
	  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	  HAL_Init();

	  /* Configure the system clock */
	  SystemClock_Config();

	  /* Initialize all configured peripherals */
	  MX_GPIO_Init();

	  while (1)
	  {
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET);
		  HAL_Delay(2000);
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET);
		  HAL_Delay(2000);
	    /* USER CODE BEGIN 3 */
	  }
	}

	/**
	  * System Clock Configuration
	  */
	void SystemClock_Config(void)
	{
	  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
	  
	  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
	  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
	  RCC_OscInitStruct.MSICalibrationValue = 0;
	  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
	  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
	    Error_Handler();
	  }

	  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
	                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
	  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
	  {
	    Error_Handler();
	  }


	  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
	  {
	    Error_Handler();
	  }
	}

	/**
	  * GPIO Initialization Function
	  */
	static void MX_GPIO_Init(void)
	{
	  GPIO_InitTypeDef GPIO_InitStruct = {0};

	  /* GPIO B Ports Clock Enable */
	  __HAL_RCC_GPIOB_CLK_ENABLE();

	  /*Configure GPIO pin Output Level */
	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13|GPIO_PIN_14, GPIO_PIN_RESET);

	  /*Configure GPIO pins : PB13 PB14 */
	  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_14;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	}

	/**
	  * This function is executed in case of error occurrence.
	  * While 1 loop that never ends. 
	  */
	void Error_Handler(void)
	{
	  /* User can add his own implementation to report the HAL error return state */
	  __disable_irq();
	  while (1)
	  {
	  }
	}

