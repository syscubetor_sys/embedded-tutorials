.. Embedded Systems documentation master file, created by
   sphinx-quick start on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============================================
Introduction to ARM Architecture 
===============================================

	Architecture defines the design of the microcontroller. 

	There are 2 major types of microcontroller architectures based on bus design.

		1) Von Neumann architecture

		2) Harvard architecture

	These architecture defines the handling of CPU memory and I/O devices.
	Von Neumann architecture is uses the same bus for Data and code memory.
	Harvard architecture is uses separate bus for Data and code memory.

	.. image:: ../../images/arm-arch/VonHar.jpg
		:align: center
		:width: 500px

	Microcontroller architecture is also divided based on the instruction set. These are again of 2 types

		1) Reduced Instruction Set Computer (RISC)

		2) Complex Instruction Set Computer (CISC)

	Very simply put, the instruction set of a CISC based microcontroller is complex. The following are the major differences between the 2

	**RISC**: Reduced Instruction Set Computer
		1. It supports powerful reduced instruction set to execute the operations within single cycle with higher clock speed.The compiler synthesizes the complicate perorations using several instructions.
		2. Length of instruction is fixed which supports the pipelining feature
		3. “LOAD” and “STORE”  instructions supports
		4. Large Number of general purpose Registers 
		5. Harvard Memory Architecture
		6. It is  prominently based on software 

	**CISC**: Complex Instruction Set Computer
		1. Complex instruction set of supports
		2. Variable length of instruction
		3. No pipeline feature
		4. Limited no of registers
		5. Von Neumann memory architecture 
		6. It is prominently based on hardware

	Some of the microcontrollers using the RISC architecture are the STM and ATmega microcontrollers. 

	CISC based microcontrollers include the 8051 and the Motorola 68000.

	ARM stands for **Advanced Risc Machine**. The ARM architecture is primarily based on the RISC architecture and are very well known for the highly efficient power usage. 

	Most chips today are based on the ARM architecture including Snapdragon and Apple Silicon chips. Broadcom chips used on the Raspberry Pi are also based on the ARM architecture. STM32 from ST Microelectronics and LPC mocrocontrollers from NXP are the most widely used microcontrolers and as usual, these are again based on the ARM architecture.

	The ARM RISC processors are designed in such way that which includes following features.

	1. Memory : Support higher memory ranges to handle the higher code density
	#. Clock frequency : They supports the wide range of frequency starting from 1MHz to 1.25GHz.Higher clock frequency which enhance the operation speed of processor. 
	#. Supports low power consumption 
	#. Uniform and fixed length instruction set support
	#. Capable to handle the FPU(Floating Point Unit) algorithms and DSP operations
	#. LOAD and STORE architecture support for data processor handling of the registers
	#. Multiple stage pipelines (e.g 3 stage - Fetch,Decode and Execute)
	#. Trust-zone technology supports for with security features.

	Additionally, ARM processor directly interact with data held by registers. Separate LOAD and STORE instructions are used to transfer the data between registers and external memory. This is done since memory operations are costly and take some time to complete. This architecture helps to copy data multiple times into the registers and use it.
	LOAD instructions move data from the memory bank to a register
	STORE instructions move data from a register to the memory banks


The ARM Family
===============================================

	The ARM processor family is divided into different type based on core implementations and supported features.Below figures describes the evolution ARM family for ARM Cortex series.

	.. image:: ../../images/arm-arch/ArmRoadMap.jpg
		:align: center
		:width: 500px

	The below figure describes the difference between ARM7,9,10 and 11 processor family based on the operation speed,pipeline feature and architecture types . 

	.. image:: ../../images/arm-arch/Arm7.jpg
		:align: center
		:width: 500px


ARM Nomenclature
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	The nomenclature is defines the processor implementation and supported features.
	To understand the nomenclature we will consider the ARM7TDMI processor family.
	List of the features:

	* x :Family or series
	* y :Memory Management/Protection Unit
	* z :Cache Memory
	* T :16 bit Thumb instruction set 
	* D :JTAG Debugger 
	* M :Fast Multiplier
	* I :Embedded In-circuit Emulator (ICE) Macrocell
	* E :Enhanced Instructions for DSP applications
	* J :Jazelle to accelerate the JAVA code execution applications
	* F :Vector Floating-point Unit arithmetic calculations
	* S :Synthesizable Version

	Explanation of the features

	**T – Thumb Instruction Set**

		The basically there are two types of instruction set supported by ARM processors 16 bit Thumb instruction set and 32 bit ARM instruction set.

	**D – JTAG Debug**

		The JATG debug support feature which is used to debug the code and flash the software into the processor.
		It is serial based protocol used between processor core and test equipment to understand the internal behavior of the processor core.

	**M – Fast Multiplier**

		To handle the complex multiplication algorithm calculation for 32 bit using minimum no of clock cycles.


	**I – Embedded ICE Macrocell**

		In build debug hardware support to set the breakpoints and watchpoints while debugging the code.
		
	**E – Enhanced Instructions**

		This features will help to deals with higher performance of DSP operations without increasing the higher clock frequency.It has extended DSP set of instruction supports.

	**J – Jazelle**

		To enhance the Jazzelle technology which handle the JAVA byte code operations.It will increase the performance of the device without affecting the battery or memory.it is used as  Jazelle DBX or Direct Byte code eXecution in smart phones.

	**F – Vector Floating-point Unit**

		It is used for Floating point algorithm calculation which is highly used in industrial real time operations.

	**S – Synthesizable**

		The ARM processor source code is supports to compiled into the different IDEs formats and which is understandable by EDA tools. 
		e.g Keil, IAR, AttolicTrueStudio, STM32CubeIDE


ARM based Embedded System Hardware
===============================================
	
	The below image describes the internal structure of the Arm based embedded system hardware which includes the different components.

.. image:: ../../images/arm-arch/ESH.jpg
	:align: center
	:width: 500px

**ARM Processor**
	It controls the embedded device. It interacts with components like memory management and caches using bus.
	It consists of core to operate the instructions and manipulates the data.
 
**Controllers**
 	It is used to handle the interrupt and memory related operations.

 **Memory Controllers**
 	It is used to deals with different types of memory(ROM,RAM) which interacts with processor bus.
 	Processor memory initializations is handled during power on reset. 

 **Interrupt Controllers**
 	The ARM processor supports two types of interrupt controllers standard and vector interrupt controllers.
 	Interrupt is the process which requires the attention from the processor to execute it.In standard interrupt controller send the signal to processor core while an external device requesting.It can be programmed to ignore or mask set of devices.The VIC is more powerful than standard because it is handles through the priority wise and ISR routine gets executed.

**Peripherals**
 	It is used to deal with input and output operations of processors with external devices.All ARM peripherals are 
 	memory mapped register support which is handled through special function registers.

**Bus**
 	The communication of different components with processor is handled through the bus.There are two types bus used in APB and AHB buses.
 	

AMBA Bus Protocol
===============================================
	AMBA is known as Advanced Micro controller Bus Architecture.All Cortex Mx series processor supports the AMBA.
	It is designed by ARM for on chip communication of the processor with different components.

		* **AHB Lite(AMBA High performance Bus)** 
		* **APB (AMBA Peripheral Bus)**

	1. AHB Lite is used as main bus interface.
	#. AHB is used to operate the communication of high speed operation peripherals(e.g DMA,SDRAM
	#. APB is used to operate the communication of low speed operation peripherals(e.g PPB ,I2C,UART)
	#. PPB :Private Peripheral Bus Region – NVIC,Sys-tick 

.. image:: ../../images/arm-arch/AMBA.jpg
	:align: center
	:width: 500px


Types of Memory
===============================================

**ROM(Read Only Memory)**
	It is non volatile memory which consist of high volume devices which do not require updates.
	It is also used to store data which required during production time.
	Boot loader code is resides into the ROM.

**Flash ROM**
	It has Read and write access but don't used for holding dynamic data.
	It holds the firmware data which is preserved after power is off.
	Erasing and writing of flash memory controlled through software and don't require any additional hardware.
	Application code will be run from the Flash ROM.

**DRAM(Dynamic Random Access Memory)**
	It has low cost compared to other RAM while accessing the data.
	The storage cells of memory need be refreshed and given electronic charge for every milliseconds before using the memory.

**SDRAM(Synchronous Dynamic Random Access Memory)**
	Much faster than the conventional RAM and provides support higher clock speed operation.It is synchronized with processor clock to fetch the data from memory cells and pipelined to deals with the burst data on the bus.

Pipeline Mechanism
===================
	The pipeline mechanism is used by RISC processor to execute the instructions.It enhances the operation speed of the processor.It handles the multiple stages fetch,decode and execute operations simultaneously while executing the multiple instructions.

	1. Fetch - Read or load the instruction from the memory
	#. Decode - Identifies the instruction to execute it
	#. Execute - Process the instruction and stores the results into the register

	As pipeline length is increases it will reduce the work at each stage and results into higher performance of processor.The system latency also increases because it takes more cycles to execute.The data dependency on certain stages increases but we can write the code using instruction scheduling information to reduce this dependency.

	In ARM9 five stage pipeline Write and execute stage added which increase the process on average 1.1 Dhyrystone MIPS per MHz an increased the instruction throughput than 13% of ARM7.

	In ARM10 six stage pipeline Issue stage is added which causes to increase the speed of process an average 1.3 Dhyrystone MIPS per MHz an increased the instruction throughput than 34% of ARM7 but also increases the  latency cost.

ARM7 Three Stage Pipeline

.. image:: ../../images/arm-arch/3stage.jpg
	:align: center
	:width: 300px

	
ARM9 Five Stage Pipeline

.. image:: ../../images/arm-arch/Arm9.jpg
	:align: center
	:width: 500px

ARM10 Six Stage Pipeline

.. image:: ../../images/arm-arch/Arm10.jpg
	:align: center
	:width: 600px



