.. Embedded Systems documentation master file, created by
   sphinx-quickstart on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Inter-Integrated Circuit(I2C)
===================================================

	I2C is multi master multi slave protocol in which one master can connect with multiple slave devices and vice versa. It uses two wires to tramsmit data
	between multiple slave devices. 

	:file:`SDA (Serial Data)` – The line for the master and slave to send and receive data.

	:file:`SCL (Serial Clock)` – The line that carries the clock signal.

.. image:: ../../images/i2c/i2c.jpg
	:align: center
	:width: 400px

I2C Bus operation speed
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	I2C can operate at below operatiing speed

.. list-table:: I2C operating speed
	:widths: 25 25
	:header-rows: 1

	* - Operating Mode
	  - Speed	

	* - Standard-mode
	  - 100 Kbit/s

	* - Full-speed
	  - 400 Kbit/s

	* - Fast-mode
	  - 1 Mbit/s

	* - High-speed
	  - 3.4 Mbit/s

Working of I2C protocol
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	I2C is based on synchronous serial communication where data is transferred bit by bit. To trnasmit data it uses SDA line while SCL is used for synchronization.

	I2C tramsfeered messages in frame format. each frame has address of salve devices. so in multi slave case data is acknowledge by the slave devices which belong to mention address in message frame.

	Each I2C frame include one start bit one stop bit read/writes bits, and ACK/NACK bits.  


	1. Start Condition: The SDA line switches from a high voltage level to a low voltage level before the SCL line switches from high to low.

	#. Stop Condition: The SDA line switches from a low voltage level to a high voltage level after the SCL line switches from low to high.

	#. Address Frame: A 7 or 10 bit sequence unique to each slave that identifies the slave when the master wants to talk to it.

	#. Read/Write Bit: A single bit specifying whether the master is sending data to the slave (low voltage level) or requesting data from it (high voltage level).

	#. ACK/NACK Bit: Each frame in a message is followed by an acknowledge/no-acknowledge bit. If an address frame or data frame was successfully received, an ACK bit is returned to the sender from the receiving device.

.. image:: ../../images/i2c/i2c_data_packet.jpg
	:align: center
	:width: 700px


Slave Addressing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	In I2C frame the master send the address of slave device it wants to communicate. This address is bradcasted to every slave device connected to master. and the slave device which belongs to that address respond with ACK bit pulling low.

Defining Read/Write opertion
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	The address bits includes a single bit at the end that informs the slave whether the master wants to perform write or receive operation. If the master wants to send data to the slave, the read/write bit is a low voltage level. If the master is requesting data from the slave, the bit is a high voltage level.

	In case of 7 bit addressing the last 8th bit signifies the READ/WRITE operation. in case of Read operation master pull READ/WRITE bit to high while for write operation the bit is low in voltage.

Data Frame
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	once the salve device ACK the address bits. master is ready to senf its first data.

	The data frame is always 8 bits long, and sent with the most significant bit first. Each data frame is immediately followed by an ACK/NACK bit to verify that the frame has been received successfully. The ACK bit must be received by either the master or the slave (depending on who is sending the data) before the next data frame can be sent.

	After all data transmission is done master can send stop condition to save device to terminate comminication.


Data Transmission
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	1. The master sends the start condition to every connected slave by switching the SDA line from a high voltage level to a low voltage level before switching the SCL line from high to low.

	#. The master sends each slave the 7 or 10 bit address of the slave it wants to communicate with, along with the read/write bit.

	#. Each slave compares the address sent from the master to its own address. If the address matches, the slave returns an ACK bit by pulling the SDA line low for one bit. If the address from the master does not match the slave’s own address, the slave leaves the SDA line high.

	#. The master sends or receives the data frame.

	#. After each data frame has been transferred, the receiving device returns another ACK bit to the sender to acknowledge successful receipt of the frame.

	#. To stop the data transmission, the master sends a stop condition to the slave by switching SCL high before switching SDA high:



.. image:: ../../images/i2c/i2c_working.jpg
	:align: center
	:width: 600px


Advantages of I2C
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	1. Only uses two wires
	#. Supports multiple masters and multiple slaves
	#. ACK/NACK bit gives confirmation that each frame is transferred successfully

Disadvantages of I2C
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	1. Slower data transfer rate than SPI
	#. The size of the data frame is limited to 8 bits
	#. More complicated hardware needed to implement than SPI

.. include:: ../../../projects/nucleo-64/baremetal/Core/app-i2c/README.rst


Reference Links
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
https://www.circuitbasics.com/basics-of-the-i2c-communication-protocol/
https://www.electronicshub.org/basics-i2c-communication/