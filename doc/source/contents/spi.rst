.. Embedded Systems documentation master file, created by
   sphinx-quickstart on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Serial Peripheral Interface(SPI)
===================================================
	SPI is a common communication protocol used by many different devices. For example, SD card modules, RFID card reader modules, and 2.4 GHz wireless transmitter/receivers all use SPI to communicate with microcontrollers.

	One unique benefit of SPI is the fact that data can be transferred without interruption. Any number of bits can be sent or received in a continuous stream. With I2C and UART, data is sent in packets, limited to a specific number of bits. Start and stop conditions define the beginning and end of each packet, so the data is interrupted during transmission.

	Devices communicating via SPI are in a master-slave relationship. The master is the controlling device (usually a microcontroller), while the slave (usually a sensor, display, or memory chip) takes instruction from the master. The simplest configuration of SPI is a single master, single slave system, but one master can control more than one slave (more on this below).

	:file:`MOSI (Master Output/Slave Input)` – Line for the master to send data to the slave.

	:file:`MISO (Master Input/Slave Output)` – Line for the slave to send data to the master.

	:file:`SCLK (Clock)` – Line for the clock signal.

	:file:`SS/CS (Slave Select/Chip Select)` – Line for the master to select which slave to send data to.

.. image:: ../../images/spi/spi.jpg
	:align: center
	:width: 400px

Clock
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	The clock signal synchronizes the output of data bits from the master to the sampling of bits by the slave. One bit of data is transferred in each clock cycle, so the speed of data transfer is determined by the frequency of the clock signal. SPI communication is always initiated by the master since the master configures and generates the clock signal.

	Any communication protocol where devices share a clock signal is known as synchronous. SPI is a synchronous communication protocol. There are also asynchronous methods that don’t use a clock signal. For example, in UART communication, both sides are set to a pre-configured baud rate that dictates the speed and timing of data transmission.

	The clock signal in SPI can be modified using the properties of clock polarity and clock phase. These two properties work together to define when the bits are output and when they are sampled. Clock polarity can be set by the master to allow for bits to be output and sampled on either the rising or falling edge of the clock cycle. Clock phase can be set for output and sampling to occur on either the first edge or second edge of the clock cycle, regardless of whether it is rising or falling.


Chip Select/Slave select
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	The master can choose which slave it wants to talk to by setting the slave’s CS/SS line to a low voltage level. In the idle, non-transmitting state, the slave select line is kept at a high voltage level. Multiple CS/SS pins may be available on the master, which allows for multiple slaves to be wired in parallel. If only one CS/SS pin is present, multiple slaves can be wired to the master by daisy-chaining.

SDO(MISO) and SDI(MOSI)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	The master sends data to the slave bit by bit, in serial through the MOSI line. The slave receives the data sent from the master at the MOSI pin. Data sent from the master to the slave is usually sent with the most significant bit first.

	The slave can also send data back to the master through the MISO line in serial. The data sent from the slave back to the master is usually sent with the least significant bit first.

Multiple slave
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	SPI can be set up to operate with a single master and a single slave, and it can be set up with multiple slaves controlled by a single master. There are two ways to connect multiple slaves to the master. If the master has multiple slave select pins, the slaves can be wired in parallel like this:

.. image:: ../../images/spi/spi_multi_slave.jpg
	:align: center
	:width: 500px

If only one slave select pin is available, the slaves can be daisy-chained like this:

.. image:: ../../images/spi/spi_daisychain.jpg
	:align: center
	:width: 500px


Data Transmission
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	1. The master outputs the clock signal.
	#. The master switches the SS/CS pin to a low voltage state, which activates the slave.
	#. The master sends the data one bit at a time to the slave along the MOSI line. The slave reads the bits 	as they are received.
	#. If a response is needed, the slave returns data one bit at a time to the master along the MISO line. The master reads the bits as they are received:

.. image:: ../../images/spi/spi_working.jpg
	:align: center
	:width: 600px


Mode of Operation of SPI
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	In SPI the master and slave has to agree some synchronization protocols. For this two feature of clock is used

	1. ``Clock Polarity(CPOL)``
		Clock Polarity determines the state of the clock. When CPOL is set to LOW, the clock SCK will be LOW in idle state and toggles to HIGH during active state (during a transfer). Similarly, when CPOL is set to HIGH, SCK will be HIGH during idle and LOW during active state.

	#. ``Clock Phase(CPHA)``
		Clock Phase determines the data transmission will be done at which clock transition i.e. rising (LOW to HIGH) or falling (HIGH to LOW). 
		When CPHA is 0, the data is transmitted on the rising edge of the clock. Data is transmitted on the falling edge when CPHA is 1.

	Based on these two feature there are four modes of operation in SPI which are shown in below table

.. list-table:: SPI mode of operation
	:widths: 25 25 25
	:header-rows: 1

	* - Operating Mode
	  - CPOL
	  - CPHA	

	* - Mode 0
	  - 0
	  - 0

	* - Mode 1
	  - 0
	  - 1

	* - Mode 2
	  - 1
	  - 0

	* - Mode 3
	  - 1
	  - 1



``Mode 0:``
	Mode 0 occurs when Clock Polarity is LOW and Clock Phase is 0 (CPOL = 0 and CPHA = 0). During Mode 0, data transmission occurs during rising edge of the clock.

``Mode 1:``
	Mode 1 occurs when Clock Polarity is LOW and Clock Phase is 1 (CPOL = 0 and CPHA = 1). During Mode 1, data transmission occurs during falling edge of the clock.

``Mode 2:``
	Mode 2 occurs when Clock Polarity is HIGH and Clock Phase is 0 (CPOL = 1 and CPHA = 0). During Mode 2, data transmission occurs during rising edge of the clock.

``Mode 3:``
	Mode 3 occurs when Clock Polarity is HIGH and Clock Phase is 1 (CPOL = 1 and CPHA = 1). During Mode 3, data transmission occurs during rising edge of the clock.

.. image:: ../../images/spi/spi_modes_and_timings.jpg
	:align: center
	:width: 500px

Advantages of SPI
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	1. No start and stop bits, so the data can be streamed continuously without interruption
	#. No complicated slave addressing system like I2C
	#. Higher data transfer rate than I2C (almost twice as fast)
	#. Separate MISO and MOSI lines, so data can be sent and received at the same time

Disadvantages of SPI
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	1. Uses four wires (I2C and UARTs use two)
	#. No acknowledgement that the data has been successfully received (I2C has this)
	#. No form of error checking like the parity bit in UART
	#. supports only single master

Working Example of SPI
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	1. Below example showcase actual working of SPI protocol.
	#. This application is based on Nucleo-64 board.

.. include:: ../../../projects/nucleo-64/baremetal/Core/app-spi/README.rst

Reference Links
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
https://www.circuitbasics.com/basics-of-the-spi-communication-protocol
https://www.electronicshub.org/basics-serial-peripheral-interface-spi/#SPI_Modes_of_Operation
