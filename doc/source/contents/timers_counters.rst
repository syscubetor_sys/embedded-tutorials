.. Embedded Systems documentation master file, created by
   sphinx-quickstart on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Timers and Counters
===================================================

A timer is a specialized type of clock which is used to measure time intervals. A timer that counts from zero upwards for measuring time elapsed is often called a stopwatch. It is a device that counts down from a specified time interval and used to generate a time delay, for example, an hourglass is a timer.

A counter is a device that stores (and sometimes displays) the number of times a particular event or process occurred, with respect to a clock signal. It is used to count the events happening outside the microcontroller. In electronics, counters can be implemented quite easily using register-type circuits such as a flip-flop.

All controllers have multiple timers. The 8051 microcontroller for example has two timers, Timer 0 and Timer 1. STM32 is more advanced and has multiple timers which can be used for gerating highly accurate PWM waveforms along with normal timer and counter operations.


Prescaler
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

All timers require a clock of some sort. Most will be connected to the microcontroller’s main CPU clock (others, like real time clocks, have their own clock sources). A timer will tick (increment by one) each time it receives a clock pulse.

.. image:: ../../images/embedded/prescaler.png
	:align: center
	:width: 400px


Timer Resolution
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Programs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Timer Example to Generate PWM
***************************************************

Timer Example
***************************************************

Counter Example
***************************************************