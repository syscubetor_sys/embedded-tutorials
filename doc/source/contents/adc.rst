.. Embedded Systems documentation master file, created by
   sphinx-quickstart on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Analog to Digital Conversion(ADC)
===================================================

	All signals in the worled are analog, whether it is the temperature of a body, the intensity of light or your voice. Unfortunately, our computers understand only the digital language of 1s and 0s. Sensors exist to get measurement based on the environment, but for the computer to be able to understand it, it has tp be first converted to a digital value. This is the basic function of Analog to Digital Convertors. Most times the value being measured is in some voltage.

	Consider a simple temperature sensor, consisting of a temperature dependent resistor. The properties of this resistor are such that when the temperature increases, the resistance decreases. Knowing this, we can connect this circuit such that we can pass some current through it and measure the voltage across it. The voltage here is an analog value. As the temperature changes, this voltage will change. This analog voltage is given to an analog to digital convertor which then gives a number corresponding to a voltage. 

	The value measure by the ADC depends on the resolution and accuracy of the ADC. If 8 pins are used, it is an 8 bit ADC and it can measure values from 0 corresponding to 0s on all 8 pins to 255 corresponding to 1s on all 8 pins. 
	i.e. 0 0 0 0 0 0 0 0  ->  1 1 1 1 1 1 1 1
	An ADC also has max voltage values. Consider 5V is the reference value given to the ADC. In this case, 0 value will correspond to 0V and 255 value will correspond to 5V. Depending on the properties of the sensor, the measured voltage will correspond to a specific temperature of the surroundings.


.. image:: ../../images/embedded/adc.png
	:align: center
	:width: 400px


Key Features of ADC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Reference Voltage
***************************************************

The digital output is the ratio of the analog input with respect to this reference voltage. It is the analog voltage value corresponding to the max digital output and  is compared to with the input to produce the digital output.

Consider Vref = 5v
This means 0 (0h) -> 0V and 255 (FFh) -> 5V
From this we can calculate the digital output for any voltage.
If input voltage is 3V 
Digital Ouput = (255 * 3)/5 = 153

Sample rate
***************************************************

The sample rate for an ADC is defined as the number of output samples taken to be converted per unit time. It is specified as samples per second. 

.. image:: ../../images/embedded/ADC-Sampling.jpg
	:align: center
	:width: 400px

For a good signal reproduction, the rate at which we sample must follow the Nyquist rate. The Nyquist Theorem states that any  analog  signal  should  be  sampled  at  least  twice  the  maximum  frequency present in that signal to achieve a faithful representation and hence there will be no loss of information.

For a controller, this rate means how many samples need to be read every second.

Resolution
***************************************************

The ADC resolution is defined as the smallest incremental voltage that can be recognized andthuscauses a change in the digital output. It is expressed as the number of bits output by the ADC.Therefore, anADC which converts the analog signal to a 8-bit digital valuehas a resolution of 8 bits.

Consider an 8 bit ADC. This means the ADC output can vary from 0 to 255. 
Assuming Vref = 5V
255 (FFh) -> 5V
Resolution = 5 / 255 = 0.019608V
This ADC can measure a change of 0.019608V.

Types Of ADCs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Successive Approximation ADCs (SAR)
***************************************************

A successive approximation A/D converter consists of a comparator, a successive approximation register (SAR), output latches, and a D/A converter.

The main part of the circuit is the 8-bit SAR, whose output is given to an 8-bit D/A converter. The analog output Va of the D/A converter is then compared to an analog signal Vin by the comparator. The output of the comparator is a serial data input to the SAR. Till the digital output (8 bits) of the SAR is equivalent to the analog input Vin, the SAR adjusts itself. The 8-bit latch at the end of conversation holds onto the resultant digital data output.

.. image:: ../../images/embedded/sar.png
	:align: center
	:width: 400px

At the start of a conversion cycle, the SAR is reset by making the start signal (S) high. The MSB of the SAR (Q7) is set as soon as the first transition from LOW to HIGH is introduced. The output is given to the D/A converter which produces an analog equivalent of the MSB and is compared with the analog input Vin.

If comparator output is LOW, D/A output will be greater than Vin and the MSB will be cleared by the SAR.
If comparator output is HIGH, D/A output will be less than Vin and the MSB will be set to the next position (Q7 to Q6) by the SAR.

According to the comparator output, the SAR will either keep or reset the Q6 bit. This process goes on until all the bits are tried. After Q0 is tried, the SAR makes the conversion complete (CC) signal HIGH to show that the parallel output lines contain valid data. The CC signal in turn enables the latch, and digital data appear at the output of the latch. As the SAR determines each bit,  digital data is also available serially. As shown in the figure above, the CC signal is connected to the start conversion input in order to convert the cycle continuously.

The biggest advantage of such a circuit is its high speed. It may be more complex than an A/D converter, but it offers better resolution.


Dual Slope A/D Converters
***************************************************

In dual slope type ADC, the integrator generates two different ramps, one with the known analog input voltage VA and another with a known reference voltage –Vref. Hence it is called a s dual slope A to D converter. The logic diagram for the same is shown below.

IMAGE

The binary counter is initially reset to 0000; the output of integrator reset to 0V and the input to the ramp generator or integrator is switched to the unknown analog input voltage VA.
The analog input voltage VA is integrated by the inverting integrator and generates a negative ramp output. The output of comparator is positive and the clock is passed through the AND gate. This results in counting up of the binary counter.


Flash A/D Converters
***************************************************

Flash analog-to-digital converters, also known as parallel ADCs, are the fastest way to convert an analog signal to a digital signal. Flash ADCs are suitable for applications requiring very large bandwidths. However, these converters consume considerable power, have relatively low resolution, and can be quite expensive. This limits them to high-frequency applications that typically cannot be addressed any other way.

.. image:: ../../images/embedded/flash-ADC.png
	:align: center
	:width: 400px

Flash ADCs are made by cascading high-speed comparators. Figure 1 shows a typical flash ADC block diagram. For an N-bit converter, the circuit employs 2N-1 comparators. A resistive-divider with 2N resistors provides the reference voltage. The reference voltage for each comparator is one least significant bit (LSB) greater than the reference voltage for the comparator immediately below it. Each comparator produces a 1 when its analog input voltage is higher than the reference voltage applied to it. Otherwise, the comparator output is 0. Thus, if the analog input is between VX4 and VX5, comparators X1 through X4 produce 1s and the remaining comparators produce 0s. The point where the code changes from ones to zeros is the point at which the input signal becomes smaller than the respective comparator reference-voltage levels.

Program
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Code::

	/* Includes ------------------------------------------------------------------*/
	#include "main.h"

	/* Private variables ---------------------------------------------------------*/
	ADC_HandleTypeDef hadc1;

	/* Private function prototypes -----------------------------------------------*/
	void SystemClock_Config(void);
	static void MX_GPIO_Init(void);
	static void MX_ADC1_Init(void);


	/**
	  * The application entry point.
	  */
	int main(void) 
	{
	  HAL_Init();

	  /* Configure the system clock */
	  SystemClock_Config();

	  /* Initialize all configured peripherals */
	  MX_GPIO_Init();
	  MX_ADC1_Init();

	  /* Infinite loop */
	  while (1)
	  {
	    uint16_t raw;
	    HAL_ADC_Start(&hadc1);
	    HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY);
	    raw = HAL_ADC_GetValue(&hadc1);
	    if (raw > 50) {
	      // High Voltage
	    } else {
	      // Low Voltage
	    }
	  }
	}

	/**
	  * System Clock Configuration
	  */
	void SystemClock_Config(void)
	{
	  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
	  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

	  /** Initializes the RCC Oscillators according to the specified parameters
	  * in the RCC_OscInitTypeDef structure.
	  */
	  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
	  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
	  RCC_OscInitStruct.MSICalibrationValue = 0;
	  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
	  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  /** Initializes the CPU, AHB and APB buses clocks
	  */
	  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
	                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
	  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
	  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
	  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_MSI;
	  PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
	  PeriphClkInit.PLLSAI1.PLLSAI1N = 16;
	  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
	  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
	  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
	  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
	  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  /** Configure the main internal regulator output voltage
	  */
	  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
	  {
	    Error_Handler();
	  }
	}

	/**
	  * ADC1 Initialization Function
	  */
	static void MX_ADC1_Init(void)
	{
	  ADC_ChannelConfTypeDef sConfig = {0};

	  /** Common config
	  */
	  hadc1.Instance = ADC1;
	  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
	  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
	  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
	  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	  hadc1.Init.LowPowerAutoWait = DISABLE;
	  hadc1.Init.ContinuousConvMode = DISABLE;
	  hadc1.Init.NbrOfConversion = 1;
	  hadc1.Init.DiscontinuousConvMode = DISABLE;
	  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	  hadc1.Init.DMAContinuousRequests = DISABLE;
	  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
	  hadc1.Init.OversamplingMode = DISABLE;
	  if (HAL_ADC_Init(&hadc1) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  
	  /** Configure Regular Channel
	  */
	  sConfig.Channel = ADC_CHANNEL_1;
	  sConfig.Rank = ADC_REGULAR_RANK_1;
	  sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
	  sConfig.SingleDiff = ADC_SINGLE_ENDED;
	  sConfig.OffsetNumber = ADC_OFFSET_NONE;
	  sConfig.Offset = 0;
	  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	  {
	    Error_Handler();
	  }
	}

	/**
	  * GPIO Initialization with clock enable Function
	  */
	static void MX_GPIO_Init(void)
	{
	  __HAL_RCC_GPIOC_CLK_ENABLE();
	}

	/**
	  * This function is executed in case of error occurrence.
	  */
	void Error_Handler(void)
	{
	  __disable_irq();
	  while (1)
	  {
	  }
	}


Programs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Reference Links
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
https://dewesoft.com/daq/types-of-adc-converters
https://www.eetimes.com/analog-to-digital-converters/