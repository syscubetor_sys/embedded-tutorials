.. Embedded Systems documentation master file, created by
   sphinx-quickstart on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Universal Asynchronous Receiver/Transmitter(UART)
===================================================
	1. UART is generally use for communicating between host and slave device.
	#. It is a asynchronous serial communication protocol where data handling is depend on baudrate and not on
	   clock :file:`eg. 4800, 9600, 19200, 115200`
	#. It is a two wire protocol where Tx pin is use to transmit while Rx is use to receive the data.
	#. UART uses start bit, stop bit, parity bit for formating data for transmittion.
	#. Before trnasmiting data receiver should know the baudrate of transmitter.
	#. Both master and salve device must configure with same settings to have successful data transfer.

.. image:: ../../images/uart/uart.jpg
	:align: center
	:width: 400px

Working of UART protocol
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	UART transmitted data is organized into packets. Each packet contains 1 start bit, 5 to 9 data bits (depending on the UART), an optional parity bit, and 1 or 2 stop bits:

	The transmitting UART adds start and stop bits to the data packet being transferred. These bits define the beginning and the end of the data packet, so that the receiving UART knows when to start reading the bits.

	When the receiving UART detects a start bit, it starts to read the incoming bits at a specific frequency known as the baud rate. Baud rate is a measure of the speed of data transfer, expressed in bits per second (bps). Both UARTs must operate at the same baud rate. 

.. image:: ../../images/uart/uart_data_packet.jpg
	:align: center
	:width: 700px

Start Bit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	The UART data transmission line is normally held at a high voltage level when it’s not transmitting data. To start the transfer of data, the transmitting UART pulls the transmission line from high to low for one clock cycle. When the receiving UART detects the high to low voltage transition, it begins reading the bits in the data frame at the frequency of the baud rate.

Data Frame
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	The data frame contains the actual data being transferred. It can be 5 bits up to 8 bits long if a parity bit is used. If no parity bit is used, the data frame can be 9 bits long. In most cases, the data is sent with the least significant bit first.

Parity Bit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Parity describes the evenness or oddness of a number. The parity bit is a way for the receiving UART to tell if any data error has occured during transmission. Bits can be changed by electromagnetic radiation, mismatched baud rates, or long distance data transfers. After the receiving UART reads the data frame, it counts the number of bits with a value of 1 and checks if the total is an even or odd number. If the parity bit is a 0 (even parity), the 1 bits in the data frame should total to an even number. If the parity bit is a 1 (odd parity), the 1 bits in the data frame should total to an odd number. When the parity bit matches the data, the UART knows that the transmission was free of errors. But if the parity bit is a 0, and the total is odd; or the parity bit is a 1, and the total is even, the UART knows that bits in the data frame have changed.

Stop Bit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	To signal the end of the data packet, the sending UART drives the data transmission line from a low voltage to a high voltage for at least two bit durations.


Advantages of UART
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	1. Only uses two wires
	#. No clock signal is necessary
	#. Has a parity bit to allow for error checking
	#. The structure of the data packet can be changed as long as both sides are set up for it

Disadvantages of UART
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	1. The size of the data frame is limited to a maximum of 9 bits
	#. Doesn’t support multiple slave or multiple master systems
	#. The baud rates of each UART must be within 10% of each other


.. include:: ../../../projects/nucleo-64/baremetal/Core/app-uart/README.rst




Reference Links
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
https://www.circuitbasics.com/basics-uart-communication/
