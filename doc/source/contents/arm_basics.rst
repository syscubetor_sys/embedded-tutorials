.. Embedded Systems documentation master file, created by
   sphinx-quickstart on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============================================
ARM Basics
===============================================


Operational modes of processor
===============================================

There are two types of operational modes of processor.

	1. Thread Mode
	
	2. Handler Mode

**Thread Mode** 
^^^^^^^^^^^^^^^^
The application code will always runs from the Thread mode which also known as "User Mode".

It is default mode of processor whenever processor starts it is always run from thread mode.

It supports both privileged and unprivileged mode.

It has privilege access mode we can put limit to access on the registers to make it unprivileged mode.

**Handler Mode**
^^^^^^^^^^^^^^^^^
The interrupt/exception code will handled through the Handler mode.

Whenever core meets with system exception or external interrupt  then core will change the mode to Handler mode in order to execute the ISR routine associated with the exception.

Handler mode always runs in the privilege access mode. 

You have full control to the processor for any resource access. 

You can access the system level registers and control level registers.


Access levels of the processor
===============================

The processor offers two access levels

	1. Privileged Access Level 

		Full read and write access to CPSR
		
		The software can use all the instructions and has access to all resources.
		
		Privileged software executes at the privileged level

	2. Unprivileged/NonPrivileged Access Level

		Only read access to CPSR 

		Limited access to MSR and MRS instruction

		cannot access the system timer, NVIC, or system control block

	If application code is running in **privileged access** mode then application has full access to  all processor specific resources and restricted registers.

	If application code is running in **unprivileged access** mode then application code don t have access to some of the processor specific restricted registers.

	By default application code is always run in privileged level.

	When the processor is in “Thread mode”, its possible to move processor  in to NAPL.

	Once you move out of the privileged to unprivileged access level  being in thread mode ,then its not possible to come back to p mode unless you change the processor operational mode to “Handler mode”.

	“Handler mode” the code execution always runs in PAL

	Use the CONTROL level register of the processor to switch between the access levels.

.. image:: ../../images/arm-arch/Thread.jpg
	:align: center
	:width: 400px


Current Program Status Register
================================

ARM processor uses the CPSR to manage the internal data operations. It is 32 bit register.This register is divided into 4 fields (flags,status,extension and control)and each field contain 8 bits of data.

.. image:: ../../images/arm-arch/CPSR.jpg
	:align: center
	:width: 500px


Processor Modes
=================

There are 7 ARM processor modes. 
	1. **Abort** - when processor failed to attempt the access memory
	2. **FIQ** - Fast interrupt request mode
	3. **IRQ** - Simple interrupt request mode
	4. **Supervisor** - After power on reset the processor will enters into the Supervisor mode 
	5. **System Mode** - Special version of user mode where full read write access to CPSR 
	6. **User Mode** - Application mode
	7. **Undefined** - when processor encounter into the unknown instruction which is not supported by implementation

.. image:: ../../images/arm-arch/Processor_modes.jpg
	:align: center
	:width: 500px


Core Registers
=====================

	The below figure describes the Cortex M Processor core registers.

.. image:: ../../images/arm-arch/CoreRegister.jpg
	:align: center
	:width: 500px

1. R0-R12 - General purpose register
#. R13 - Stack Pointer
#. R14 - Linked Register
#. R15 - Program counter
#. PSR - Program Status Register
#. Exception Mask Register
#. Control Register

**General purpose register**
	R0-R12 registers used as general purpose register for data operations.

**Stack Pointer**
	R13 register used as stack pointer which holds the address of Main stack pointer and process stack pointer.
	In Thread mode, bit[1] of the CONTROL register indicates the stack pointer to use.For MSP(0) and PSP(1).
	On reset, the processor loads the MSP with the value from address 0x00000000.

**Link Register**
	R14 register used as stack pointer. It stores the return address of subroutines, function calls, and exceptions.On reset,the processor sets the LR value to 0xFFFFFFFF.

**Program Counter**
	R15 register used as Program counter. It contains the current program address. 
	On reset,the processor loads the PC with the value of the reset vector, which is at address 0x00000004.
	This is also known as Reset handler address where main function of application will start to execute. 
	
**Program Status Register**
	The Program Status Register (PSR) is combinations of below registers:
	1. Application Program Status Register (APSR)
	#. Interrupt Program Status Register (IPSR)
	#. Execution Program Status Register (EPSR).

.. image:: ../../images/arm-arch/PSR.jpg
	:align: center
	:width: 500px

To access this register we can use the MRS and MSR instruction.

.. image:: ../../images/arm-arch/PSR_All.jpg
	:align: center
	:width: 300px

**Application Program Status Register (APSR)**
	This status register contains the condition flags(Negative,Zero,Carry and Overflow) status from previous instruction execution.

.. image:: ../../images/arm-arch/APSR.jpg
	:align: center
	:width: 300px

**Interrupt Program Status Register**
	The IPSR contains the details of exception handling.e.g ISR numbers

.. image:: ../../images/arm-arch/IPSR.jpg
	:align: center
	:width: 400px

**Execution Program Status Register**
	The EPSR contains the Thumb state bit, and the execution state bits for (IT) instruction
	and Interruptible-Continuable Instruction (ICI) field for an interrupted load multiple or store multiple instruction.

.. image:: ../../images/arm-arch/EPSR_1.jpg
	:align: center
	:width: 400px
.. image:: ../../images/arm-arch/EPSR_2.jpg
	:align: center
	:width: 400px
	

Importance of T bit 
=====================
	1. Various ARM Processors support ARM-Thumb inter working mechanism which handles the ability to switch between ARM and Thumb state.
	#. The processor must be in ARM state to execute the instruction of ARM ISA and In thumb state it will handle Thumb ISA
	#. If T bit of the EPSR set (1) then processor consider that the next instruction which it is about to execute  from Thumb ISA.
	#. If T bit of  the EPSR reset (0 )then processor consider that the next instruction which it is about to execute from ARM  ISA.
	#. The cortex Mx processor doesn’t support the ARM state. Hence the value of ‘T’ bit always be 1.
	#. Failing to maintain this is illegal and this will result in the “Usage Fault” exception.


Reset Sequence of Processor
============================

	1. When you reset the processor then program counter is loaded with memory value 0x000000
	#. Then processor reads the value @0x0000000 and copies data into MSP(Main Stack pointer)
	   MSP holds the address of Flash memory/ROM.Processor first  initializes the stack pointer.
	#. Processor reads the value at 0x00000004 into the PC i.e actual address of Reset handler.
 	#. PC jumps to reset handler.
	#. Reset handler is C or assembly function Code written by you to carry out any initialization required.(e.g System Init)
	#. From reset handler you will call your main() function.

Memory Mapping
==============


.. image:: ../../images/arm-arch/Memory_Map.jpg
	:align: center
	:width: 400px


Please refer the below explanation for different memory regions of processor.

**Code Region**
	This region contains the programming flash data.All the application code will runs from this region.Processor fetches the vector table information from this region after reset.Different types of code memories like embedded flash,OTP,ROM and EEPROM etc.(size 512MB -)

**SRAM Region**
	It is connected to on chip SRAM.The first 1 MB bytes of regions is bit addressable.We can also execute the code from this region.

**Peripheral Region**
	It is used for on chip peripherals(e.g GPOI).Like SRAM region first 1MB of region is bit addressable.

**External RAM Region**
	It is used to connecting external SDRAM.This region is intended for either on-chip or off-chip memory. The accesses are cache able, and you can execute code in this region.

**External Device Region**
	This region is intended for external devices and/or shared memory that needs ordering/non-buffered accesses.This region is eXecute never region.

**System Region**
	This region is for private peripherals and vendor-specific devices. It is non executable.
	e.g NVIC,System Timer,System control block.


Reference Links
================

https://doc.lagout.org/electronics/Game%20boy%20advance/ARM_BOOKS/ARM_System_Developers_Guide-Designing_and_Optimizing_System_Software.pdf

https://community.arm.com/developer/ip-products/processors/b/processors-ip-blog/posts/a-tour-of-the-cortex-m3-core

https://static.docs.arm.com/dui0553/a/DUI0553A_cortex_m4_dgug.pdf


