.. Embedded Systems documentation master file, created by
   sphinx-quickstart on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============================================
Programming an ARM based Embedded System
===============================================
	This sections describes the steps involved in programming for embedded system using ARM GNU tool chain.
	We can compile the C code without using IDEs. There are different GNU commands are used to generate the file formats like .obj,.map,.lst,.bin,.hex etc. 

Compilation stages
===============================================
	Application source file go through 4 different stages of compilation which are explain below

.. image:: ../../images/cross-compilation/GCC_CompilationProcess.webp
	:align: center
	:width: 500px

**Preprocessing**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	It is the first stage of compilation process. All preprocessing directive,symbols and macros are replaced by compiler with predefined values .e.g #define  
	Expansion of included header files and removes the comments from the code.
	e.g "main.c"-"main.i"

**Compiling** 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	It is the second stage of compilation process.It compiles the preprocessed code and generates the code in assembly language file format.The syntax checking and code optimization is done by this stage.
	e.g "main.i"-"main.s"

**Assembling**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	It is the third stage of compilation process. It takes the input as assembly language code and generate the object code.
	e.g "main.s"-"main.o"

**Linking** 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	It is the last stage of compilation process where all the object files collected to generate the single executable file. The linking of all library functions and symbol table is handled through linker stage.
	e.g "main.o"-"main.ld"
	There are different formats like .bin,.axf are generated in this stage.


Cross Compilation
===============================================
	It is the process in which cross compilation tool chain will runs on PC and generate the executable file format for different target arm architecture.

	e.g Keil,IAR,GNU

	Cross compilation tool chains contains all the binary files which is required to generate different files formats for preprocessing,compiling,assembling and linking stage.
	We can able to analyze the each and every generated file using disassembling,conversion of bin file into hex,memory allocation for each sections (.bss,.data,.code) using .map file

	e.g GNU Arm Tool chains

Important Compiler 
===============================================

	Compiler-Linker-Assembler
	 **arm-none-eabi-gcc**

	Linker
	 **arm-none-eabi-ld**

	Assembler
	 **arm-none-eabi-as**

	Elf file analyzer
		1. **arm-none-eabi-objdump**
		#. **arm-none-eabi-readelf**
		#. **arm-none-eabi-nm**

	Format converter
	 **arm-none-eabi-objcopy**

Make file Concept
===============================================
	Using Makefile multiple source file can be compiled at a single glance. It is the simplest way to compile the code using **make** command. We can generate the different file formats as well as we can delete the generated files using **make clean** command.

.. image:: ../../images/cross-compilation/Make1.jpg
	:align: center
	:width: 500px

Analysis of Relocatable Object file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The object file will be generated by GCC tool is in executable and linkable format.
It contains the information for organized memory sections like .bss,.data,.code etc.

The content of object file can be viewed using objdump command.

To generate the objdump file
 arm-none-eabi-objdump.exe -h  main.o

.. image:: ../../images/cross-compilation/Make2.jpg
	:align: center
	:width: 500px

To display the full contents of file
  arm-none-eabi-objdump.exe -s  main.o

.. image:: ../../images/cross-compilation/Make3.jpg
	:align: center
	:width: 500px

To generate the log output file
 arm-none-eabi-objdump.exe -d  main.o  >  main_log.txt

.. image:: ../../images/cross-compilation/Make4.jpg
	:align: center
	:width: 500px

Reference Links
===============================================
https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads

https://gcc.gnu.org/onlinedocs/gcc/index.html#SEC_Contents

