===================================================
Interaction With Peripherals
===================================================

	In this section we will talk about different wired communication protocol
	which is widley use in Embedded Systems Application development.

.. toctree::
   :titlesonly:

   gpio.rst
   adc.rst
   dac.rst
   timers_counters.rst
   interrupts.rst
   rtc.rst
   uart.rst
   i2c.rst
   spi.rst
   dma.rst