.. Embedded Systems documentation master file, created by
   sphinx-quickstart on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Interrupts
===================================================


What is an Interrupt
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An interrupt is a signal to the processor or controller emitted after some event on hardware or software, indicating that the event needs immediate attention.

Whenever the interrupt signal occurs, the controller pauses the tasks that it is currently completing and starts executing the functions reuqired to handle the events. This function is called the an Interrupt Service Routine (ISR) or Interrupt Handler. ISR tells the processor or controller what to do when the interrupt occurs. When the ISR is completed, the task that is paused is resumed.


What is Polling?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The first way to check for events is to continuously check for signals. It is like continuosly looking at the clock every minute. It take CPU unnecessary cycles and these cycles are often wasted when there is no event. This method of continuousy checking for a signal is known as polling.

The microcontroller keeps checking for for the signals. For example consider a switch connected to a GPIO pin. When the switch is on, 5V appears on the pin. Polling means that you continuously read the GPIO pin and the moment you read a 5V on the pin, you execute the required function. As you can see, a lot of unnecesary cycles get wasted. Hence the requirement for interrupts. In the interrupt method, the controller responds only when an interruption occurs. Thus, the controller is not required to regularly monitor the status of any pins.


Types Of Interrupts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	1) Level Triggered Interrupt: A level-triggered interrupt module generates an interrupt when and while the interrupt source is asserted.

	2) Edge Triggered Interrupt


Steps to Execute an Interrupt
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When an interrupt gets active, the microcontroller goes through the following steps −

1) The microcontroller closes the currently executing instruction and saves the address of the next instruction (PC) on the stack.

2) It also saves the current status of all the interrupts internally (i.e., not on the stack).

3) It jumps to the memory location of the interrupt vector table that holds the address of the interrupts service routine.

4) The microcontroller gets the address of the ISR from the interrupt vector table and jumps to it. It starts to execute the interrupt service subroutine, which is RETI (return from interrupt).

5) Upon executing the RETI instruction, the microcontroller returns to the location where it was interrupted. First, it gets the program counter (PC) address from the stack by popping the top bytes of the stack into the PC. Then, it start to execute from that address.


Interrupt Vector Table
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
An interrupt vector table (IVT) is a data structure that associates a list of interrupt handlers with a list of interrupt requests in a table of interrupt vectors. Each entry of the interrupt vector table, called an interrupt vector, is the address of an interrupt handler.


Program 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

