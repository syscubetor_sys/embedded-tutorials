.. Embedded Systems documentation master file, created by
   sphinx-quickstart on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Digital to Analog Conversion (DAC)
===================================================

A Digital to Analog Converter (DAC) converts a digital input signal into an analog output signal. 

.. image:: ../../images/embedded/dac.png
	:align: center
	:width: 400px

Types of DAC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1) Weighted Resistor DAC

Digital-to-Analogue Converters, or DAC’s as they are more commonly known, are the opposite of the Analogue-to-Digital Converters we looked at in a previous tutorial. DAC’s convert binary or non-binary numbers and codes into analogue ones with its output voltage (or current) being proportional to the value of its digital input number. For example, we may have a 4-bit digital logic circuit that ranges from 0000 to 11112, (0 to F16) which a DAC converts to a voltage output ranging from 0 to 10V.

Converting an “n”-bit digital input code into an equivalent analogue output voltage between 0 and some VMAX value can be done in a number of ways, but the most common and easily understood conversion methods uses a weighted resistors and a summing amplifier, or a R-2R resistor ladder network and operational amplifier. Both digital-to-analogue conversion methods produce a weighted sum output, with the weights set by the resistive values used in the ladder networks contributing a different “weighted” amount to the signals output.

We saw in our tutorial section about Operational Amplifiers that an inverting amplifier uses negative feedback to reduce its open-loop gain, AOL and does so by feeding back a fraction of its output signal back to the input. We also saw that the input voltage VIN is connected directly to its inverting input via a resistor RIN and that the inverting amplifiers closed-loop voltage gain, AV(CL) is determined by the ratio of these two resistors as shown.


.. image:: ../../images/embedded/weight-dac.jpg
	:align: center
	:width: 400px


2) R-2R Ladder DAC

R-2R Digital-to-Analogue Converter, or DAC, is a data converter which use two precision resistor to convert a digital binary number into an analogue output signal proportional to the value of the digital number.

The R-2R resistive ladder network uses just two resistor values, one which is the base value “R” and the other which has twice the value, “2R” of the first resistor no matter how many bits are used to make up the ladder network. So for example, we could just use a 1kΩ resistor for the base resistor “R”, and therefore a 2kΩ resistor for “2R” (or multiples thereof as the base value of R is not too critical), thus 2R is always twice the value of R, that is 2R = 2*R. This means that it is much easier to maintain the required accuracy of the resistors along the ladder network compared to the previous weighted resistor DAC. But what is a “R-2R resistive ladder network” anyway.

.. image:: ../../images/embedded/dac-r2r.png
	:align: center
	:width: 400px


Program
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^