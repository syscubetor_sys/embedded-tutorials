.. Embedded Systems documentation master file, created by
   sphinx-quickstart on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============================================
Introduction to Embedded Systems 
===============================================

What is Embedded Systems
===============================================

	Embedded system is a system consisting of hardware and software which is designed to perform specific dedicated task. 
	The software in this case is embedded into the designed hardware. Microcontrollers or Microprocessors, form the brain of the system and the system consists of various peripherals like sensors for sensing the environment and actuators to take actions based on the received data.

	A microprocessor is the brain of the system. It is the central processing unit which handles all the computations and the decision making of the system. A microcontroller on the other hand consists of a CPU, Memory and I/O all integrated into one chip. Essentially it is a microcontroller with external chips for memory and peripheral interface circuits. Most Embedded System today are based on microcontrollers.

	The embedded software which is nothing but the Application/driver helps in instructing hadware to perform specific task.


.. image:: ../../images/embedded.webp
	:align: center
	:width: 500px

Embedded System Characterisitcs
===============================================

	Embedded systems are designed to do some specific task, rather than be a general-purpose computer for multiple tasks. Some also have real-time performance constraints that must be met, for reasons such as safety and usability; others may have low or no performance requirements, allowing the system hardware to be simplified to reduce costs.

	Embedded systems are not always standalone devices. Many embedded systems consist of small parts within a larger device that serves a more general purpose. For example, an embedded system in an automobile provides a specific function as a subsystem of the car itself.

	The program instructions written for embedded systems are referred to as firmware, and are stored in read-only memory or flash memory chips. They run with limited computer hardware resources: little memory, small or non-existent keyboard or screen.

Embedded System Applications
===============================================
	
	Use of Embedded system are commonly found in diffrent domain like Semiconductor, Automotive, Telecommunication, Aerospace, IoT, Robotics etc.

	Almost all products have some form of embedded systems inside them. Most commonly used embedded systems range from a common digital watch to control systems implemented on the International Space Station.

	Major industries like Intel, Qualcomm, Texas Instruments, NXP Semiconductor,
	ST Microelectronics, Analog devices rely heavily on Embedded Systems.


Peripherals
===============================================

	Embedded systems talk with the outside world via peripherals, such as:

	1. Serial Communication Interfaces (SCI): RS-232, RS-422, RS-485, etc.
	#. Synchronous Serial Communication Interface: I2C, SPI, SSC and ESSI (Enhanced Synchronous Serial Interface)
	#. Universal Serial Bus (USB)
	#. Multi Media Cards (SD cards, Compact Flash, etc.)
	#. Networks: Ethernet, LonWorks, etc.
	#. Fieldbuses: CAN-Bus, LIN-Bus, PROFIBUS, etc.
	#. Timers: PLL(s), Capture/Compare and Time Processing Units
	#. Discrete IO: aka General Purpose Input/Output (GPIO)
	#. Analog to Digital/Digital to Analog (ADC/DAC)
	#. Debugging: JTAG, ISP, BDM Port, BITP, and DB9 ports.


Reference Links
===============================================
https://en.wikipedia.org/wiki/Embedded_system#Applications

