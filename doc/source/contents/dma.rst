.. Embedded Systems documentation master file, created by
   sphinx-quickstart on Tue Feb 18 21:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Direct Memory Access (DMA)
===================================================

Direct memory access (DMA) is a method that allows an input/output (I/O) device to send or receive data directly to or from the main memory, bypassing the CPU to speed up memory operations.

The process is managed by a chip known as a DMA controller 

.. image:: ../../images/embedded/dma.png
	:align: center
	:width: 400px

Consider you want to send some data to a speaker. Voice messages consist of a large number of bytes to for a few seconds in order to maintain the clarity of the message. If the controller sends this data directly, the CPU will be busy for a long time. This is the perfect use case to use a DMA. The CPU will store all the data in some location. It will then inform the DMA controller of the starting address and the bumber of bytes to be transferred. This means that now the CPU is free to handle other tasks or interrupts that may come up.


DMA Channels
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Programs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^