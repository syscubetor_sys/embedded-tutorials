.. syscubetor documentation master file, created by
   sphinx-quickstart on Fri Feb 21 13:55:05 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============================================
Tutorials
===============================================

Contents:

.. toctree::
   :titlesonly:

   contents/embedded.rst
   contents/arm_architecture.rst
   contents/arm_basics.rst
   contents/bare_metal.rst
   contents/comm_protocol.rst


.. Indices and tables
.. ==================
..
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

